
var keystone = require('keystone');
var async = require('async');



exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'account';

	// Load the galleries by sortOrder
	view.query('account', keystone.list('Account').model.find().sort('sortOrder'));

	// Render the view
	view.render('account');

};
