var express = require('express');
var router = express.Router();
var userDao = require('../sql/userDao');

/* GET home page. */
router.get('/info', function(req, res, next) {
  res.render('admin/personal/info', { title: 'info' });
});
router.get('/home', function(req, res, next) {
  res.render('admin/personal/home', { title: 'home' });
});

router.get('/user/login', function(req, res, next) {
    console.log('登录'+req);
    
    userDao.login(req, res, next);
    
    
});
module.exports = router;
