var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'home' });
});

router.get('/signup',function(req,res,next){
  res.render('login/signup',{ title: 'signup' })

});

router.get('/walls',function(req,res,next){
  res.render('walls',{ title: 'walls' })


});
router.get('/footer',function(req,res,next){
  res.render('footer',{ title: 'walls' })


});
router.get('/test',function(req,res,next){
  res.render('test',{ title: 'walls' })


});

router.get('/back',function(req,res,next){
  res.render('login/back',{ title: 'back' })
});

router.get('/admin',function(req,res,next){
  res.render('admin/admin',{ title: 'admin' })
});

module.exports = router;
