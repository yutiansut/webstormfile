// sql/SqlMapping.js
// CRUD SQL语句
// 这个脚本主要用于封装sql语句  以sql-开头
// sqlUser 用于打包关于登录系统的语句
// sqlNodeOrder 用于打包关于视频源列表顺序的语句
// sqlNodeList  用于打包关于输出节点
//

module.exports={

  user: {
    insert:"INSERT INTO user(name, password) " + " VALUES(?,?)",
    update:'update user set name=?, password=? where id=?',
    updatename:'update users set name=?, age=? where id=?',
    delete: 'delete from users where id=?',
    queryAll: 'SELECT * from user',
    login:'SELECT * from user where name=? and password=?'
  },
  
  video: {
    insert:'INSERT INTO user(video) VALUES(?,?) where name=?',
    update:'update user set video=? where name=?'
  },
  
  nodes:{
    insert:'INSERT INTO user(nodes) VALUES(?,?) where name=?',
    update:'update user set nodes=? where name=?'
  }
  

};
