// dao/userDao.js
// 实现与MySQL交互
var mysql = require('mysql');
var $conf = require('../conf/dbConnection');
var $util = require('../util/util');
var $sql = require('./SqlMapping');
var sqlsplit = require('yutiansut-sql-split');
var express = require('express');
var router = express.Router();
// 使用连接池，提升性能
var pool = mysql.createPool($conf.mysql);


// 向前台返回JSON方法的简单封装

var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.json({
            code: '1',
            msg: '操作失败'
        });
    } else {
        res.json(ret);
    }
};

module.exports = {
    add: function (req, res, next) {
        pool.getConnection(function (err, connection) {

            /**connection.config.queryFormat = function (query, values) {
                if (!values) return query;
                  return query.replace(/\:(\w+)/g, function (txt, key) {
                      if (values.hasOwnProperty(key)) {
                        return this.escape(values[key]);
                        }
                        return txt;
                      }.bind(this));
                    };*/

            var param = req.query;

            var inserts = [param.name, param.age];

            var sqlq = mysql.format($sql.user.insert, inserts);
            console.log('sqlq' + sqlq)
            //connection.config.queryFormat = sqlsplit.yutiansutsqlsplit (sqlq, values) ;

            connection.config.queryFormat = function (query, values) {
                if (!values) return query;
                return query.replace(/\:(\w+)/g, function (txt, key) {
                    if (values.hasOwnProperty(key)) {
                        return this.escape(values[key]);
                    }
                    return txt;

                }.bind(this));
            };
            console.log(sqlq)
            // 建立连接，向表中插入值
            // 'INSERT INTO user(id, name, age) VALUES(0,?,?)',
            connection.query(sqlq, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '增加成功'
                    };
                }
                // 以json形式，把操作结果返回给前台页面
                jsonWrite(res, result);

                // 释放连接
                connection.release();
            });
        });
    },
    login: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            var param = req.query;
            var name = param.name;
            var pass = param.password;
            var sqlq = mysql.format($sql.user.login, [name, pass]);
            console.log('sqlq' + sqlq)
            console.log('NAME' + name+'pass'+pass)
            //connection.config.queryFormat = sqlsplit.yutiansutsqlsplit (sqlq, values) ;
            connection.config.queryFormat = function (query, values) {
                if (!values) return query;
                return query.replace(/\:(\w+)/g, function (txt, key) {
                    if (values.hasOwnProperty(key)) {
                        return this.escape(values[key]);
                    }
                    return txt;

                }.bind(this));
            };
            console.log(sqlq)

            connection.query(sqlq, function (err, result) {
           
             /** if (result) {
                    result = {
                        code: 200,
                        msg: '登录成功'
                    };
                }*/
                // 以json形式，把操作结果返回给前台页面
                var strings1=JSON.stringify(result);
                var strings2=JSON.parse(strings1);
                console.log('strings1'+strings1)
                console.log('strings2'+strings2)
                
                jsonWrite(res, strings2);
               // console.log(res);

                // 释放连接
                connection.release();
            });
        });
    },
    delete: function (req, res, next) {
        // delete by Id
        pool.getConnection(function (err, connection) {
            var id = +req.query.id;
            connection.query($sql.user.delete, id, function (err, result) {
                if (result.affectedRows > 0) {
                    result = {
                        code: 200,
                        msg: '删除成功'
                    };
                } else {
                    result = void 0;
                }
                
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    update: function (req, res, next) {
        // update by id
        // 为了简单，要求同时传name和age两个参数
        var param = req.body;
        if (param.name == null || param.age == null || param.id == null) {
            jsonWrite(res, undefined);
            return;
        }

        pool.getConnection(function (err, connection) {
            connection.query($sql.user.update, [param.name, param.age, +param.id], function (err, result) {
                // 使用页面进行跳转提示
                if (result.affectedRows > 0) {
                    res.render('suc', {
                        result: result
                    }); // 第二个参数可以直接在jade中使用
                } else {
                    res.render('fail', {
                        result: result
                    });
                }
                console.log(result);

                connection.release();
            });
        });

    },
    queryById: function (req, res, next) {
        var id = +req.query.id; // 为了拼凑正确的sql语句，这里要转下整数
        console.log(id)
        pool.getConnection(function (err, connection) {
            console.log($sql.user.queryById)
            connection.query($sql.user.queryById, id, function (err, result) {
                jsonWrite(res, result);
                connection.release();

            });
        });
    },
    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) throw err;
            connection.query($sql.user.queryAll, function (err, result) {
                if (err) throw err;
                jsonWrite(res, result);
                connection.release();
            });
        });
    }
};
