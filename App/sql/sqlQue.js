// 实现与MySQL交互
var mysql = require('mysql');
var $conf = require('../conf/dbConnection');
//var $util = require('../util/util');
var $sql = require('./sqlMapping');
//About 引入mysql pool 的意义 ：
// 在长时间的连接时候，出于性能的考虑 mysql会down掉关于mysql的connection 这时候再去请求 会出现连接错误的问题
// 解决办法就是使用mysql pool
var pool = mysql.createPool($conf.mysql);
var connection = mysql.createConnection($conf.mysql);


// 向前台返回JSON方法的简单封装
var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.json({
            code: '1',
            msg: '操作失败'
        });
    } else {

        res.json(ret);
    }
};





module.exports = {
    queryAll: function (req, res, next) {


        pool.getConnection(function (err, connection) {
            connection.query('SELECT * from userlist', function (err, result) {
                jsonWrite(res, result);
                //console.log(result);
                connection.release();
            });
        });
    },

    login: function (req, res, next) {


        pool.getConnection(function (err, connection) {
            connection.query('SELECT * from user where name=', function (err, result) {
                jsonWrite(res, result);
                //console.log(result);
                connection.release();
            });
        });
    }



};
