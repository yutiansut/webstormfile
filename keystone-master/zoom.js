/**
 * Created by yutia on 2016/3/30.
 */
var zoomRate = 20;//每次放缩比例增量
var maxRate = 300;//最大放大倍数
var minRate = 20;//最小缩小倍数
var currZoom = 100;//当前缩放比
var dvWidth = 200;//构造缩放toolbar的宽度
var dvHeight = 30;///构造缩放toolbar的高度
var cname = '';
//--------------cookie--------------------------//
//将以前选择的zoom存入cookie//
function SetCookie(name, value) {
	var exdate = new Date()
	exdate.setDate(exdate.getDate() + 30)
	document.cookie = name + "=" + escape(value) + ";expires=" + exdate.toGMTString();
	// document.cookie = name + "=" + escape(value) + ";";
}
function GetCookie(name) {
	if (document.cookie.length > 0) {
		var start = document.cookie.indexOf(name + "=")
		if (start != -1) {
			start = start + name.length + 1
			var end = document.cookie.indexOf(";", start)
			if (end == -1) end = document.cookie.length
			return unescape(document.cookie.substring(start, end))
		}
	}
	return ""
}
//---------------------Zoom------------------------//
function Zoom() {
//在已有的页面动态加载div，用来放入功能控件
	var dv = document.createElement('div');
	dv.setAttribute('id', 'btnDiv');
	dv.style.position = "absolute";
	dv.style.display = "block";
	dv.style.left = 0;
	dv.style.top = 0;
	dv.style.width = dvWidth;
	dv.style.height = dvHeight;
	dv.style.border = "2px inset gray";
	dv.style.backgroundColor = "transparent";
	dv.innerHTML = "ButtonArea";
	document.body.appendChild(dv);
//添加功能控件
	var select = '<select id="showZoom"  onchange="changeZoom();"><option value="20%">20%</option><option value="40%">40%</option><option value="60%">60%</option><option value="80%">80%</option>'
		+'<option value="100%" selected="selected">100%</option><option value="120%">120%</option><option value="140%">140%</option><option value="160%">160%</option>'
		+'<option value="180%">180%</option><option value="200%">200%</option><option value="220%">220%</option><option value="240%">240%</option><option value="260%">260%</option>'
		+ '<option value="280%">280%</option><option value="300%">300%</option></select>';
	document.getElementById('btnDiv').innerHTML = '<table id="btnTb"><tr id="btnTr"> <td style=" border:1px inset black;" mce_style=" border:1px inset black;"><input id="btnZoomIn" type="button" value="  放大(+)" onclick="ZoomIn();" style="background:url(ZoomIn.png);background-repeat:no-repeat;text-align:right:" mce_style="background:url(ZoomIn.png);background-repeat:no-repeat;text-align:right:"/></td>'
		+ '<td style=" text-align:center;vertical-align:middle;border:1px inset black;" mce_style=" text-align:center;vertical-align:middle;border:1px inset black;">' + select + '</td> <td style=" text-align:center;border:1px inset black;" mce_style=" text-align:center;border:1px inset black;"><input id="btnZoomOut" type="button"value="  缩小(-)"  onclick="ZoomOut();"  style="background:url(ZoomOut.png);background-repeat:no-repeat;text-align:right:" mce_style="background:url(ZoomOut.png);background-repeat:no-repeat;text-align:right:"/></td></tr></table>';

	if (GetCookie("zoomVal") != null && GetCookie("zoomVal") != "") {
		currZoom = GetCookie("zoomVal");
	}
	else {
		currZoom = '100%';
	}

	showZoom(currZoom);
}
//支持键盘事件，实现点击加减号缩放
function changeZoom() {
	currZoom = document.getElementById('showZoom').value;
	showZoom(currZoom);
}
document.onkeypress = function checkzoom(e) {
	switch (event.keyCode) {
		case 61:
			ZoomIn();
			break;
		case 45:
			ZoomOut();
			break;
		default:
			break;
	}
}
//放大
function ZoomIn() {
	if (parseInt(currZoom) >= maxRate) {
		return;
	}
	else {
		currZoom = parseInt(document.body.style.zoom) + zoomRate + '%';
	}
	showZoom(currZoom);
}
//缩小
function ZoomOut() {
	if (parseInt(currZoom) <= minRate) {
		return;
	}
	else {
		currZoom = parseInt(document.body.style.zoom) - zoomRate + '%'
	}
	showZoom(currZoom);
}
//恢复正常显示 100%
function initZoom(contentid) {
	currZoom = '100%';
	document.body.style.zoom = currZoom;
	document.getElementById('showZoom').value = currZoom;
}
function showZoom(zoomVal) {
	//resizeControls(zoomVal);
	document.body.style.zoom = zoomVal;
	var dv = document.getElementById('btnDiv');
//保持div控件大小不变
	dv.style.zoom = parseInt(10000 / parseFloat(zoomVal))+'%';
	document.getElementById('showZoom').selectedIndex = (parseFloat(zoomVal) - minRate) / zoomRate;
	SetCookie("zoomVal", zoomVal);
}
//------------------move------------------------------//
//移动
document.onmousedown = function() {
	document.body.style.cursor = "move";
	var dv = document.getElementById('btnDiv');
	var e = arguments[0] || event;
	var x = document.body.scrollLeft + e.clientX;
	var y = document.body.scrollTop + e.clientY;
	// document.body.setCapture();
	document.onmousemove = function() {
		scrollTo(x - e.clientX, y - e.clientY);
//保持toolbar始终停靠在页面右上端
		dv.style.left = document.body.scrollLeft + 'px';
		dv.style.top = document.body.scrollTop + 'px';
		return false;
	}
	document.onmouseup = function() {
		//document.body.releaseCapture();
		document.body.style.cursor = "default";
		document.onmousemove = null;
	}
	return false;
}
