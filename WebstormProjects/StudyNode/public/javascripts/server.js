/**
 * Created by yutia on 2016/3/28.
 */
var http = require("http");
exports.start = function(){
    http.createServer(function(request, response) {
        console.log("Request received...");
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Hello node.js");
        response.end();
    }).listen(8888);
    console.log("server start...");
}