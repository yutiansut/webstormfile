
/**
 * Created by yutia on 2016/3/27.
 */

// app.js
var connect = require("./lib/connect") ;
var user = require("./models/user") ;
var app = connect.createServer() ;
app .use(connect.logger("dev"))
    .use(connect.query())
    .use(connect.bodyParser())
    .use(connect.cookieParser())
    .use(connect.static(__dirname + "/views"))
    .use(connect.static(__dirname + "/public"))
    .use("/login",function(request,response,next){
      var name = request.body["name"] ;
      var password = request.body["password"] ;
      user.login(name,password,function(user){
        if(user){
          response.end("Welcome to:" + user["name"] + " ^_^ ... ...") ;
        }
        else{
          response.end("User:" + name + " Not Register !")    ;
        }
      }) ;
    })
    .use("/reg",function(request,response,next){
      var name = request.body["name"] ;
      var password = request.body["password"] ;
      new user({
        name : name ,
        password : password
      }).save(function(user){
        if(user && user["name"]){
          response.end("User:" + name + "Register Done !")    ;
        }
        else{
          response.end("User: " + name + "has registed !") ;
        }
      }) ;
    })
    .listen(8888,function(){
      console.log("Web Server Running On Port ---> 8888 .") ;
    }) ;