/**
 * Created by yutia on 2016/3/27.
 */

var conn = require("../lib/ ") ;
function User(user){
    this.name = user["name"] ;
    this.password = user["password"] ;
} ;
User.prototype.save = function(callback){
    var that = this ;
    conn.collection("users",{
        safe : true
    },function(error,collection){
        if(error) return conn.close() ;
        collection.findOne({   // 判断此用户是否存在
            name : that.name
        },function(error,user){
            if(error) return conn.close() ;
            if(!user){
                collection.insert({
                    name : that.name + "" ,
                    password : that.password + ""
                },{
                    safe : true
                },function(error,user){
                    if(error) return conn.close() ;
                    callback && callback(user) ;
                    conn.close() ;
                }) ;
            }
            else{
                callback("User has registed !") ;
            }
        }) ;
    }) ;
} ;
User.login = function(name,password,callback){
    conn.collection("users",{
        safe : true
    },function(error,collection){
        if(error) return conn.close() ;
        collection.findOne({
            name : name ,
            password : password
        },function(error,user){
            if(error) return conn.close() ;
            callback && callback(user) ;
            conn.close() ;
        }) ;
    }) ;
} ;
exports = module.exports = User ;

