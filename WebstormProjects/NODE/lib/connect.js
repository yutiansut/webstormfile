/**
 * Created by yutia on 2016/3/27.
 */

var mongodb = require("mongodb") ;
var server = new mongodb.Server("localhost",27017,{
    auto_reconnect : true
}) ;
var conn = new mongodb.Db("bb",server,{
    safe : true
}) ;
conn.open(function(error,db){
    if(error) throw error ;
    console.info("mongodb connected !") ;
}) ;
exports = module.exports = conn ;